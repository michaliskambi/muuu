#!/bin/bash
set -eu

. /usr/local/fpclazarus/bin/setup.sh default

export CASTLE_ENGINE_PATH=/var/lib/jenkins/workspace/castle_game_engine_build/
# add castle-engine tool to $PATH
export PATH="${CASTLE_ENGINE_PATH}"tools/build-tool/:"${PATH}"

# clean previous build artifacts (otherwise they would be left in case
# version changes in tar.gz/zip name, or they would be left in case
# some tool fails to create new ones but exits with 0 status).
rm -f muuu-*.tar.gz \
      muuu-*.zip \
      muuu*.apk

castle-engine auto-generate-textures

castle-engine package --os=win64 --cpu=x86_64 --verbose
castle-engine package --os=win32 --cpu=i386 --verbose
castle-engine package --os=linux --cpu=x86_64 --verbose

bash <<EOF
set -eu
. /usr/local/android/android_setup.sh
castle-engine package --os=android --cpu=arm --verbose
EOF

echo '---- Build OK.'
