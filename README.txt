Fun 2D game about cows and alien spaceships:)

For desktop and mobile (Android).
Using Castle Game Engine http://castle-engine.sourceforge.net/ .
May be a nice example of a not-so-trivial 2D game using Castle Game Engine API
like TGLImage and TSprite.

Open-source. License: GNU GPL >= 2.

By Michalis Kamburelis.
Enjoy!
Done in 1 day (2 + 6 + 4 work hours now):)
