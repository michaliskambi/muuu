{
  Copyright 2016-2022 Michalis Kamburelis.

  This file is part of "Muuu".

  "Muuu" is free software; see the file COPYING.txt,
  included in this distribution, for details about the copyright.

  "Muuu" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  ----------------------------------------------------------------------------
}

{ Player. }
unit GamePlayer;

interface

uses CastleFilesUtils, CastleUtils, CastleSceneManager,
  CastleColors, CastleGLImages, CastleVectors, CastleRectangles,
  CastleTimeUtils, CastleUIControls;

type
  TPlayer = class
  public
    Sprite: TSprite;
    AnimationIdleWithGun,
      AnimationWalkWithGun,
      AnimationShoot,
      AnimationJumpShoot: Integer;
    Position: TVector2;
    Left: boolean;
    LastRocketTime: TFloatTime;
    constructor Create;
    destructor Destroy; override;
    procedure Update(const SecondsPassed: TFloatTime);
  end;

  { Layer where player is drawn. }
  TPlayerLayer = class(TCastleUserInterface)
    procedure Render; override;
  end;

var
  Player: TPlayer;

  MouseInput: boolean =
    {$ifdef ANDROID} false {$else}
    {$ifdef iOS}     false {$else}
                     true {$endif} {$endif};

implementation

uses SysUtils, Math,
  CastleKeysMouse,
  GameViewport, GameRockets, GameUtils;

constructor TPlayer.Create;
begin
  inherited;

  Sprite := TSprite.CreateFrameSize(
    'castle-data:/cowboy/cowboy4.png',
    26, 8, 49, 54, false);
  Sprite.HorizontalSpacing := 1;
  Sprite.VerticalSpacing := 1;
  Sprite.FramesPerSecond := 8;

  AnimationIdleWithGun := Sprite.AddAnimation([0, 1, 2, 3]);
  AnimationShoot := Sprite.AddAnimation([4, 5, 6, 7]);
  AnimationWalkWithGun := Sprite.AddAnimation([8, 9, 10, 11]);
  AnimationJumpShoot := Sprite.AddAnimation([12, 13, 14, 15]);
  // AnimationJumpWithGun := Sprite.AddAnimation([16]);
  // AnimationIdleWithoutGun := Sprite.AddAnimation([17, 18, 19, 20]);
  // AnimationWalkWithoutGun := Sprite.AddAnimation([21, 22, 23, 24]);
  // AnimationJumpWithoutGun := Sprite.AddAnimation([25]);

  Sprite.SwitchToAnimation(AnimationIdleWithGun);
  Sprite.Play;
end;

destructor TPlayer.Destroy;
begin
  FreeAndNil(Sprite);
  inherited;
end;

procedure TPlayer.Update(const SecondsPassed: TFloatTime);
var
  LastWalk, LastShoot: boolean;

  procedure Walk(const ALeft: boolean);
  var
    X: Single;
  begin
    Left := ALeft;
    LastWalk := true;
    X := SecondsPassed * 500;
    if Left then
      X := -X;
    Position.X += X;
  end;

  procedure Shoot(const WindowTarget: TVector2);
  const
    RocketDelay = 0.2;
  var
    RocketPos, RocketDir: TVector2;
  begin
    if LastRocketTime + RocketDelay < WorldTime then
    begin
      RocketPos := Position;
      RocketPos.Y += 30;
      RocketDir := (WindowToViewport(WindowTarget) - Position).Normalize;
      RocketPos.X += Sign(RocketDir.X) * 120;
      Rockets.FireRocket(RocketPos, RocketDir);
      LastRocketTime := WorldTime;
      { if not walking, adjust Left based on shooting }
      if not LastWalk then
        Left := RocketDir.X < 0;
    end;
    LastShoot := true;
  end;

var
  WantedAnimation, I: Integer;
  TouchPos: TVector2;
begin
  Sprite.Update(SecondsPassed);
  LastWalk := false;
  LastShoot := false;

  if MouseInput then
  begin
    if Window.Pressed[keyArrowLeft] then
      Walk(true);
    if Window.Pressed[keyArrowRight] then
      Walk(false);
    if buttonLeft in Window.MousePressed then
      Walk(Window.MousePosition[0] < Window.Width / 2);
    if buttonRight in Window.MousePressed then
      Shoot(Window.MousePosition);
  end else
  begin
    { all walks }
    for I := 0 to Window.Container.TouchesCount - 1 do
    begin
      TouchPos := Window.Container.Touches[I].Position;
      if WindowToViewport(TouchPos)[1] < GrassY then
        Walk(TouchPos[0] < Window.Width / 2);
    end;
    { all shoots, so that walk determines Left with priority }
    for I := 0 to Window.Container.TouchesCount - 1 do
    begin
      TouchPos := Window.Container.Touches[I].Position;
      if WindowToViewport(TouchPos)[1] > GrassY then
        Shoot(TouchPos);
    end;
  end;

  WantedAnimation := AnimationIdleWithGun;
  if LastWalk and LastShoot then
    WantedAnimation := AnimationJumpShoot
  else
  if LastWalk then
    WantedAnimation := AnimationWalkWithGun
  else
  if LastShoot then
    WantedAnimation := AnimationShoot
  else
    WantedAnimation := AnimationIdleWithGun;
  if Sprite.CurrentAnimation <> WantedAnimation then
    Sprite.SwitchToAnimation(WantedAnimation);
end;

procedure TPlayerLayer.Render;
const
  PlayerViewportWidth = 300;
  PlayerViewportHeight = 300;
  PlayerMiddle = PlayerViewportWidth / 4;
var
  R: TFloatRectangle;
begin
  inherited;
  R := ViewportToWindow(
    FloatRectangle(
      Player.Position[0] - Iff(Player.Left, PlayerViewportWidth - PlayerMiddle, PlayerMiddle),
      Player.Position[1],
      PlayerViewportWidth,
      PlayerViewportHeight));
  Player.Sprite.DrawFlipped(R, Player.Left, false);
end;

end.
