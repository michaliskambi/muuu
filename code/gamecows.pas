{
  Copyright 2016-2022 Michalis Kamburelis.

  This file is part of "Muuu".

  "Muuu" is free software; see the file COPYING.txt,
  included in this distribution, for details about the copyright.

  "Muuu" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  ----------------------------------------------------------------------------
}

{ Cows. }
unit GameCows;

interface

uses Generics.Collections, Classes,
  CastleUIControls, CastleGLImages, CastleVectors, CastleTimeUtils,
  CastleRectangles;

type
  TCow = class
  strict private
    function Flies: boolean;
    function MilkRect: TFloatRectangle;
  public
    Visible: boolean;
    Position, PositionWhenAbductingBegins: TVector2;
    SpawnTime, AbductionAppearTime, AbductionBeamTime: TFloatTime;
    Image: TDrawableImage;
    ScaleWhenStanding, Scale, Rotation: Single;
    AbductionVisible: boolean;
    AbductionPosition: TVector2;
    Milk: Single;
    { Clear the state to initialize new instance.
      Use this to create new instance, instead of destroying + creating
      new class instance, for max speed. }
    procedure Initialize(const X: Single);
    procedure Update(const SecondsPassed: TFloatTime);
    procedure Render(var CowsInViewport: Cardinal);
    function Rect: TFloatRectangle;
    function AbductionRect: TFloatRectangle;
    procedure AbductionReset;
    function AbductionPointCollides(const P: TVector2): boolean;
    function AbductionProgress: Single;
  end;

  TCowList = specialize TObjectList<TCow>;

  TCows = class(TCastleUserInterface)
  private
    FVisibleCount: Integer;
    ImageCow1, ImageCow2, ImageAbductionShip, ImageAbductionBeam,
      ImageMilkFull, ImageMilkEmpty: TDrawableImage;
    LastCowSpawn: TFloatTime;
    CowsInViewport: Cardinal;
    procedure SpawnCow(const X: Single);
  public
    List: TCowList;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Render; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: boolean); override;
    procedure StartGame;
  end;

var
  Cows: TCows;

implementation

uses SysUtils, Math,
  CastleFilesUtils, CastleKeysMouse, CastleLog, CastleTransform, CastleUtils,
  CastleImages,
  GameViewport, GameExplosions, GameUtils, GamePlayer;

const
  BeamDuration = 2;
  AbductionScale = 0.75;

{ TCow -------------------------------------------------------------------- }

procedure TCow.Initialize(const X: Single);
begin
  Visible := true;
  Position := Vector2(X, ViewportHeight + 100);
  SpawnTime := WorldTime;
  ScaleWhenStanding := RandomFloatRange(0.9, 1.1);
  Scale := 1; // will be really set correctly in nearest Update
  Rotation := 0;
  if Random < 0.5 then
    Image := Cows.ImageCow1
  else
    Image := Cows.ImageCow2;
  AbductionReset;
end;

procedure TCow.Update(const SecondsPassed: TFloatTime);

  function FullyAbductedPosition: TVector2;
  const
    FullyAbductedShift: TVector2 = (X: -280; Y: 570);
  begin
    Result := Vector2(PositionWhenAbductingBegins.X, GrassY) +
      FullyAbductedShift * AbductionScale;
  end;

  function DirectionToFullyAbducted: TVector2;
  begin
    Result := (FullyAbductedPosition - Position).Normalize;
  end;

  function MilkIncreaseSpeed: Single;
  const
    MilkIncreaseSpeedFast = 1 / 4;
    MilkIncreaseSpeedFastDistance = 100;
    MilkIncreaseSpeedSlow = 1 / 16;
    MilkIncreaseSpeedSlowDistance = 500;
  begin
    Result := MapRangeClamped(
      Abs(Player.Position.X - Position.X),
      MilkIncreaseSpeedSlowDistance, MilkIncreaseSpeedFastDistance,
      MilkIncreaseSpeedSlow        , MilkIncreaseSpeedFast);
  end;

const
  CowFallSpeed = 600;
  AbductionShipFallSpeed = 800;
  AbductionPullSpeed = 200;
  CowRotationSpeed = 10;
  AbductionTargetY = GrassY;
begin
  { update Position, Rotation, Milk }
  if AbductionVisible and (AbductionBeamTime <> 0) then
  begin
    Position += DirectionToFullyAbducted * SecondsPassed * AbductionPullSpeed;
    Rotation := 0;
    Milk := 0;
  end else
  begin
    if not Flies then
    begin
      Rotation := 0;
      Milk += SecondsPassed * MilkIncreaseSpeed;
      if Milk > 1 then
      begin
        { hooray - cow milked! }
        Visible := false;
        Score += 100;
        Exit;
      end;
    end else
    begin
      Rotation += SecondsPassed * CowRotationSpeed;
      Milk := 0;
    end;
    Position.Y := Max(GrassY, Position.Y - SecondsPassed * CowFallSpeed);
  end;

  { update Scale based on Position }
  Scale := MapRange(Position.Y,
    GrassY, ViewportHeight, ScaleWhenStanding, ScaleWhenStanding * 0.5);

  if (not AbductionVisible) and (WorldTime > AbductionAppearTime) then
    AbductionVisible := true;

  if AbductionVisible then
  begin
    AbductionPosition.Y := Max(AbductionTargetY,
      AbductionPosition.Y - SecondsPassed * AbductionShipFallSpeed);

    if (AbductionPosition.Y = AbductionTargetY) and
       (AbductionBeamTime = 0) then
      AbductionBeamTime := WorldTime;

    if (AbductionBeamTime <> 0) and
       (WorldTime > AbductionBeamTime + BeamDuration) then
      Visible := false;
  end;
end;

procedure TCow.Render(var CowsInViewport: Cardinal);
var
  AbductionR, CowR, MilkR, MilkInsideR: TFloatRectangle;
  AbductionInViewport: boolean;
  MilkHeightMultiplier: Single;
begin
  if AbductionVisible then
  begin
    AbductionR := ViewportToWindow(AbductionRect);
    AbductionInViewport := FloatRectangle(Window.Rect).Collides(AbductionR);
    if AbductionInViewport then
      Cows.ImageAbductionShip.Draw(AbductionR);
  end else
    AbductionInViewport := false;

  CowR := ViewportToWindow(Rect);
  // Grow is a hack to keep CowsInViewport including newly spawned cows
  if FloatRectangle(Window.Rect).Collides(CowR.Grow(0, 500)) then
  begin
    Image.Rotation := Rotation;
    Image.Draw(CowR);
    Inc(CowsInViewport);

    if not Flies then
    begin
      MilkR := ViewportToWindow(MilkRect);
      Cows.ImageMilkEmpty.Draw(MilkR);

      MilkHeightMultiplier := MapRange(Milk, 0, 1, 0.1, 0.9);
      MilkR.Height := MilkHeightMultiplier * MilkR.Height;
      MilkInsideR := FloatRectangle(Cows.ImageMilkEmpty.Rect);
      MilkInsideR.Height := MilkHeightMultiplier * MilkInsideR.Height;
      Cows.ImageMilkFull.Draw(MilkR, MilkInsideR);
    end;
  end;

  if AbductionInViewport and (AbductionBeamTime <> 0) then
  begin
    Cows.ImageAbductionBeam.Color := Vector4(1, 1, 1, AbductionProgress);
    Cows.ImageAbductionBeam.Draw(AbductionR);
  end;
end;

function TCow.Flies: boolean;
begin
  Result := Position.Y > GrassY;
end;

function TCow.Rect: TFloatRectangle;
begin
  Result := FloatRectangle(
    Position.X,
    Position.Y,
    Image.Width * Scale,
    Image.Height * Scale);
end;

function TCow.MilkRect: TFloatRectangle;
const
  MilkScale = 1;
begin
  Result := FloatRectangle(
    Position.X +
      (Image.Width * Scale - Cows.ImageMilkFull.Width * MilkScale) * 0.5,
    Position.Y + 180,
    Cows.ImageMilkFull.Width * MilkScale,
    Cows.ImageMilkFull.Height * MilkScale);
end;

function TCow.AbductionRect: TFloatRectangle;
begin
  Result := FloatRectangle(
    AbductionPosition.X,
    AbductionPosition.Y,
    Cows.ImageAbductionShip.Width * AbductionScale,
    Cows.ImageAbductionShip.Height * AbductionScale);
end;

procedure TCow.AbductionReset;
begin
  AbductionVisible := false;
  { note: give at least enough time to fall down }
  AbductionAppearTime := WorldTime + RandomFloatRange(3, 5);
  AbductionBeamTime := 0;

  AbductionPosition := Position;
  AbductionPosition.X -= 300;
  AbductionPosition.Y := ViewportHeight + 200;

  PositionWhenAbductingBegins := Position;
end;

function TCow.AbductionPointCollides(const P: TVector2): boolean;
var
  ARect: TFloatRectangle;
  AImage: TCastleImage;
  ImageX, ImageY: Integer;
begin
  ARect := AbductionRect;
  AImage := Cows.ImageAbductionShip.Image as TCastleImage;
  ImageX := Round(MapRange(P.X, ARect.Left, ARect.Right, 0, AImage.Width));
  ImageY := Round(MapRange(P.Y, ARect.Bottom, ARect.Top, 0, AImage.Height));
  Result :=
    Between(ImageX, 0, AImage.Width - 1) and
    Between(ImageY, 0, AImage.Height - 1) and
    // opaque pixel
    (AImage.Colors[ImageX, ImageY, 0][3] >= 0.5);
end;

function TCow.AbductionProgress: Single;
begin
  Result := (WorldTime - AbductionBeamTime) / BeamDuration;
end;

{ TCows ------------------------------------------------------------------ }

constructor TCows.Create(AOwner: TComponent);
begin
  inherited;
  List := TCowList.Create(true);
  ImageCow1 := TDrawableImage.Create('castle-data:/cow1.png');
  ImageCow2 := TDrawableImage.Create('castle-data:/cow2.png');
  ImageAbductionShip := TDrawableImage.Create('castle-data:/abduction_ship.png');
  if not (ImageAbductionShip.Image is TCastleImage) then
    raise Exception.CreateFmt('Abduction image "%s" must not be GPU compressed',
      [ImageAbductionShip.Image.URL]);
  ImageAbductionBeam := TDrawableImage.Create('castle-data:/abduction_beam.png');
  ImageMilkFull := TDrawableImage.Create('castle-data:/milk_full.png');
  ImageMilkEmpty := TDrawableImage.Create('castle-data:/milk_empty.png');
end;

destructor TCows.Destroy;
begin
  FreeAndNil(List);
  FreeAndNil(ImageCow1);
  FreeAndNil(ImageCow2);
  FreeAndNil(ImageAbductionShip);
  FreeAndNil(ImageAbductionBeam);
  FreeAndNil(ImageMilkFull);
  FreeAndNil(ImageMilkEmpty);
  inherited;
end;

procedure TCows.Render;
var
  I: Integer;
begin
  inherited;
  CowsInViewport := 0;
  for I := 0 to List.Count - 1 do
    if List[I].Visible then
      List[I].Render(CowsInViewport);
end;

procedure TCows.SpawnCow(const X: Single);
const
  PoolIncrease = 10;
var
  I, OldCount: Integer;
begin
  Inc(FVisibleCount);

  for I := 0 to List.Count - 1 do
    { use first available instance }
    if not List[I].Visible then
    begin
      List[I].Initialize(X);
      Exit;
    end;

  OldCount := List.Count;
  List.Count := List.Count + PoolIncrease;
  WritelnWarning('Cows', Format('Pool size %d is too small, increasing to %d',
    [OldCount, List.Count]));
  for I := OldCount to List.Count - 1 do
    List[I] := TCow.Create;
  List[OldCount].Initialize(X);
end;

procedure TCows.Update(const SecondsPassed: Single; var HandleInput: boolean);
const
  CowSpawnDelay = 0.33;
  MaxCowViewport = 4;
var
  I: Integer;
begin
  inherited;

  if (LastCowSpawn + CowSpawnDelay < WorldTime) and
     (CowsInViewport < MaxCowViewport) then
  begin
    SpawnCow(Player.Position.X - ViewportWidth / 2 + Random * ViewportWidth);
    LastCowSpawn := WorldTime;
  end;

  for I := 0 to List.Count - 1 do
    if List[I].Visible then
      List[I].Update(SecondsPassed);
end;

procedure TCows.StartGame;
const
  PoolSize = 100;
var
  I: Integer;
begin
  List.Count := PoolSize;
  FVisibleCount := 0;
  for I := 0 to List.Count - 1 do
    { this will also destroy previous List[I],
      if StartGame is not called for 1st time.
      And that's Ok, we want it. }
    List[I] := TCow.Create;
end;

end.
