{
  Copyright 2016-2022 Michalis Kamburelis.

  This file is part of "Muuu".

  "Muuu" is free software; see the file COPYING.txt,
  included in this distribution, for details about the copyright.

  "Muuu" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  ----------------------------------------------------------------------------
}

{ Backgrounds. }
unit GameBackground;

interface

uses SysUtils, Math,
  CastleControls, CastleFilesUtils,
  CastleUtils, CastleUIControls, CastleColors,
  CastleGLImages, CastleVectors, CastleRectangles, CastleGLUtils,
  CastleTimeUtils;

type
  TBackgroundLayer = class(TCastleUserInterface)
  strict protected
    const
      BackgroundParallax = 0.75;
      TreesParallax = 0.5;
      GrassParallax = 0.25;
    procedure DrawRepeatingImage(const Image: TDrawableImage;
      const Parallax, Y, AHeight: Single;
      const ScaleDistance: Single = 1.0);
  end;

  TUnder = class(TBackgroundLayer)
  public
    procedure Render; override;
  end;

  TAbove = class(TBackgroundLayer)
  public
    procedure Render; override;
  end;

var
  SimpleBackground: TCastleRectangleControl;
  ImageGrassForeground, ImageGrassBackground, ImageBgHills, ImageBgCity,
    ImageTree: TDrawableImage;

implementation

uses GameViewport, GamePlayer, GameUtils;

{ TBackgroundLayer ----------------------------------------------------------- }

procedure TBackgroundLayer.DrawRepeatingImage(const Image: TDrawableImage;
  const Parallax, Y, AHeight: Single;
  const ScaleDistance: Single = 1.0);
var
  X, ImageViewportWidth: Double;
begin
  X := Player.Position[0] * Parallax;
  ImageViewportWidth := AHeight * Image.Width / Image.Height;
  while X >= ScreenLeftBottom[0] do
    X -= ImageViewportWidth * ScaleDistance;
  while X + ImageViewportWidth < ScreenLeftBottom[0] do
    X += ImageViewportWidth * ScaleDistance;
  while X < ScreenLeftBottom[0] + ViewportWidth do
  begin
    Image.Draw(ViewportToWindow(FloatRectangle(
      X, Y, ImageViewportWidth, AHeight)));
    X += ImageViewportWidth * ScaleDistance;
  end;
end;

{ TUnder --------------------------------------------------------------------- }

procedure TUnder.Render;

  procedure DrawBackground;
  var
    Image: TDrawableImage;
  begin
    if InCity then
      Image := ImageBgCity
    else
      Image := ImageBgHills;
    DrawRepeatingImage(Image, BackgroundParallax, 0, ViewportHeight);
  end;

  procedure DrawGrassBackground;
  begin
    DrawRepeatingImage(ImageGrassBackground, GrassParallax, GrassY, 50, 0.8);
  end;

  procedure DrawTrees;
  begin
    DrawRepeatingImage(ImageTree, TreesParallax, GrassY - 50, 800, 3.0);
  end;

begin
  inherited;
  DrawBackground;
  DrawGrassBackground;
  DrawTrees;
end;

{ TAbove --------------------------------------------------------------------- }

procedure TAbove.Render;

  procedure DrawGrassForeground;
  begin
    DrawRepeatingImage(ImageGrassForeground, GrassParallax, GrassY, 50, 0.8);
  end;

  procedure DrawGrassUnder;
  begin
    DrawRectangle(Rectangle(0, 0, Window.Width, Ceil(GrassY * ViewportScale)),
      HexToColor('063c06'));
  end;

begin
  inherited;
  DrawGrassForeground;
  DrawGrassUnder;
end;

end.
