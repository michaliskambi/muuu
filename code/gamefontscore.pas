{
  Copyright 2016-2017 Michalis Kamburelis.

  This file is part of "Muuu".

  "Muuu" is free software; see the file COPYING.txt,
  included in this distribution, for details about the copyright.

  "Muuu" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  ----------------------------------------------------------------------------
}

{ Game score font class (TFontScore). }
unit GameFontScore;

interface

uses CastleFonts;

type
  { Score font. The score font is a little special
    (contains only numbers, not letters)
    so it needs a little special class to be measured correctly. }
  TFontScore = class(TTextureFont)
    { The "score" font has only letters, misses other chars.
      So default calculation of RowHeight and friends doesn't work. }
    procedure Measure(out ARowHeight, ARowHeightBase, ADescend: Single); override;
  end;

var
  FontScore: TFontScore;

implementation

procedure TFontScore.Measure(out ARowHeight, ARowHeightBase, ADescend: Single);
begin
  ARowHeight := 220; // this is font letter height, assuming font size = 300
  ARowHeight *= Size / 300; // adjust to current Size
  ARowHeightBase := ARowHeight;
  ADescend := 0;
end;

end.
