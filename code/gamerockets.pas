{
  Copyright 2016-2017 Michalis Kamburelis.

  This file is part of "Muuu".

  "Muuu" is free software; see the file COPYING.txt,
  included in this distribution, for details about the copyright.

  "Muuu" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  ----------------------------------------------------------------------------
}

{ Rockets. }
unit GameRockets;

interface

uses Generics.Collections, Classes,
  CastleUIControls, CastleGLImages, CastleVectors, CastleTimeUtils,
  CastleRectangles;

type
  TRocket = class
    Visible: boolean;
    Position: TVector2; //< Position of the middle of the image
    Direction: TVector2; //< Direction, must be normalized.
    { Rotation in radians, suitable for TGLImage or TSprite, derived from Direction. }
    Rotation: Single;
    ExplosionTime: TFloatTime;
    { Clear the state to initialize new instance.
      Use this to create new instance, instead of destroying + creating
      new class instance, for max speed. }
    procedure Initialize(const APosition, ADirection: TVector2);
    procedure Update(const SecondsPassed: TFloatTime);
    procedure Render;
    function Rect: TFloatRectangle;
  end;

  TRocketList = specialize TObjectList<TRocket>;

  TRockets = class(TCastleUserInterface)
  private
    FVisibleCount: Integer;
    Image: TGLImage;
    List: TRocketList;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Render; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: boolean); override;
    procedure StartGame;
    { Fire rocket with given parameters.
      Direction will be normalized inside, don't worry about it. }
    procedure FireRocket(const Position, Direction: TVector2);
  end;

var
  Rockets: TRockets;

implementation

uses SysUtils, Math,
  CastleFilesUtils, CastleKeysMouse, CastleLog, CastleTransform, CastleUtils,
  GameViewport, GameExplosions, GameUtils, GameCows;

{ TRocket -------------------------------------------------------------------- }

procedure TRocket.Initialize(const APosition, ADirection: TVector2);
begin
  Visible := true;
  Position := APosition;
  Direction := ADirection.Normalize;
  // our graphics is already rotated by Pi / 2, i.e. it points up (not right)
  Rotation := ArcTan2(Direction[1], Direction[0]) - Pi / 2;
end;

procedure TRocket.Render;
var
  ScreenRect: TFloatRectangle;
begin
  ScreenRect := ViewportToWindow(Rect);
  if FloatRectangle(Window.Rect).Collides(ScreenRect) then
  begin
    Rockets.Image.Rotation := Rotation;
    Rockets.Image.Draw(ScreenRect);
  end else
  begin
    Visible := false;
    Dec(Rockets.FVisibleCount);
  end;
end;

procedure TRocket.Update(const SecondsPassed: TFloatTime);
const
  RocketSpeed = 1000;
var
  J: Integer;
  C: TCow;
  RocketRect: TFloatRectangle;
begin
  RocketRect := Rect;
  for J := 0 to Cows.List.Count - 1 do
  begin
    C := Cows.List[J];
    (* Too difficult, make cows invulnerable.
    if C.Visible and
      { adding Grow(-20) to make it harder to hit cow }
      C.Rect.Collides(RocketRect.Grow(-20)) and
      C.Flies then
    begin
      Explosions.Explode(Position, Rotation);
      Visible := false;
      C.Visible := false;
      Dec(FVisibleCount);
      Score -= 100;
      Break;
    end;
    *)
    if C.Visible and
      C.AbductionVisible and
      C.AbductionRect.Collides(RocketRect) and
      C.AbductionPointCollides(RocketRect.Center) then
    begin
      { adding Direction * 20 just makes explosion look better }
      Explosions.Explode(Position + Direction * 20, Rotation);
      Visible := false;
      C.AbductionReset;
      Dec(Rockets.FVisibleCount);
      Score += 10;
      Break;
    end;
  end;
  if Visible then // if still flying
    Position += Direction * SecondsPassed * RocketSpeed;
end;

function TRocket.Rect: TFloatRectangle;
const
  RocketScale = 0.5;
begin
  Result := FloatRectangle(
    Position[0],
    Position[1],
    Rockets.Image.Width * RocketScale,
    Rockets.Image.Height * RocketScale);
end;

{ TRockets ------------------------------------------------------------------ }

constructor TRockets.Create(AOwner: TComponent);
begin
  inherited;
  List := TRocketList.Create(true);
  Image := TGLImage.Create(ApplicationData('ornamented_arrow_0.png'));
end;

destructor TRockets.Destroy;
begin
  FreeAndNil(List);
  FreeAndNil(Image);
  inherited;
end;

procedure TRockets.Render;
var
  I: Integer;
begin
  inherited;
  for I := 0 to List.Count - 1 do
    if List[I].Visible then
      List[I].Render;
end;

procedure TRockets.FireRocket(const Position, Direction: TVector2);
const
  PoolIncrease = 10;
var
  I, OldCount: Integer;
begin
  Inc(FVisibleCount);

  for I := 0 to List.Count - 1 do
    { use first available instance }
    if not List[I].Visible then
    begin
      List[I].Initialize(Position, Direction);
      Exit;
    end;

  OldCount := List.Count;
  List.Count := List.Count + PoolIncrease;
  WritelnWarning('Rockets', Format('Pool size %d is too small, increasing to %d',
    [OldCount, List.Count]));
  for I := OldCount to List.Count - 1 do
    List[I] := TRocket.Create;
  List[OldCount].Initialize(Position, Direction);
end;

procedure TRockets.Update(const SecondsPassed: Single;
  var HandleInput: boolean);
var
  I: Integer;
begin
  inherited;
  for I := 0 to List.Count - 1 do
    if List[I].Visible then
      List[I].Update(SecondsPassed);
end;

procedure TRockets.StartGame;
const
  PoolSize = 100;
var
  I: Integer;
begin
  List.Count := PoolSize;
  FVisibleCount := 0;
  for I := 0 to List.Count - 1 do
    { this will also destroy previous List[I],
      if StartGame is not called for 1st time.
      And that's Ok, we want it. }
    List[I] := TRocket.Create;
end;

end.
