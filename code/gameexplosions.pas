{
  Copyright 2016-2017 Michalis Kamburelis.

  This file is part of "Muuu".

  "Muuu" is free software; see the file COPYING.txt,
  included in this distribution, for details about the copyright.

  "Muuu" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  ----------------------------------------------------------------------------
}

{ Explosions. }
unit GameExplosions;

interface

uses Generics.Collections, Classes,
  CastleUIControls, CastleGLImages, CastleVectors, CastleTimeUtils,
  CastleRectangles,
  GameUtils;

type
  TExplosion = class
    Visible: boolean;
    Position: TVector2; //< position of the middle of the image
    { Rotation in radians, suitable for TGLImage or TSprite, derived from Direction. }
    Rotation: Single;
    ExplosionTime: TFloatTime;
    { Clear the state to initialize new instance.
      Use this to create new instance, instead of destroying + creating
      new class instance, for max speed. }
    procedure Initialize(const APosition: TVector2; const ARotation: Single);
    procedure Render;
    function Rect: TFloatRectangle;
  end;

  TExplosionList = specialize TObjectList<TExplosion>;

  TExplosions = class(TCastleUserInterface)
  private
    FVisibleCount: Integer;
    FList: TExplosionList;
    FExplosion: TGLVideo2D;
    property List: TExplosionList read FList;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Render; override;
    procedure GLContextOpen; override;
    procedure GLContextClose; override;
    procedure StartGame;
    procedure Explode(const Position: TVector2; const Rotation: Single);
  end;

var
  Explosions: TExplosions;

implementation

uses SysUtils, Math,
  CastleFilesUtils, CastleKeysMouse, CastleLog, CastleOpenDocument,
  GameViewport;

{ TExplosion -------------------------------------------------------------------- }

procedure TExplosion.Initialize(const APosition: TVector2; const ARotation: Single);
begin
  Visible := true;
  ExplosionTime := WorldTime;
  Position := APosition;
  Rotation := ARotation;
end;

procedure TExplosion.Render;
const
  ExplosionScale = 2;
var
  Image: TGLImageCore;
begin
  if WorldTime < ExplosionTime + Explosions.FExplosion.Duration then
  begin
    Image := Explosions.FExplosion.GLImageFromTime(WorldTime - ExplosionTime);
    Image.Rotation := Rotation;
    Image.Draw(ViewportToWindow(Rect));
  end else
  begin
    { it's dirty, but fast, to change Visible state here,
      no need to iterate in Update(). }
    Visible := false;
    Dec(Explosions.FVisibleCount);
  end;
end;

function TExplosion.Rect: TFloatRectangle;
const
  ExplosionScale = 2;
begin
  Result := FloatRectangle(
    Position[0],
    Position[1],
    Explosions.FExplosion.Width * ExplosionScale,
    Explosions.FExplosion.Height * ExplosionScale);
  Result.Left   := Result.Left   - Result.Width  / 2;
  Result.Bottom := Result.Bottom - Result.Height / 2;
end;

{ TExplosions ------------------------------------------------------------------ }

constructor TExplosions.Create(AOwner: TComponent);
begin
  inherited;
  FList := TExplosionList.Create(true);
end;

destructor TExplosions.Destroy;
begin
  FreeAndNil(FList);
  inherited;
end;

procedure TExplosions.Render;
var
  I: Integer;
begin
  inherited;
  for I := 0 to List.Count - 1 do
    if List[I].Visible then
      List[I].Render;
end;

procedure TExplosions.GLContextOpen;
begin
  inherited;
  FExplosion := TGLVideo2D.Create(ApplicationData('explosion_320x240_frameskip2/explosion_1@counter(4).png'), false);
end;

procedure TExplosions.GLContextClose;
begin
  FreeAndNil(FExplosion);
  inherited;
end;

procedure TExplosions.Explode(const Position: TVector2; const Rotation: Single);
const
  PoolIncrease = 10;
var
  I, OldCount: Integer;
begin
  Vibrate(100);

  Inc(FVisibleCount);

  for I := 0 to List.Count - 1 do
    { use first available instance }
    if not List[I].Visible then
    begin
      List[I].Initialize(Position, Rotation);
      Exit;
    end;

  OldCount := List.Count;
  List.Count := List.Count + PoolIncrease;
  WritelnWarning('Explosions', Format('Pool size %d is too small, increasing to %d',
    [OldCount, List.Count]));
  for I := OldCount to List.Count - 1 do
    List[I] := TExplosion.Create;
  List[OldCount].Initialize(Position, Rotation);
end;

procedure TExplosions.StartGame;
const
  PoolSize = 400;
var
  I: Integer;
begin
  List.Count := PoolSize;
  FVisibleCount := 0;
  for I := 0 to List.Count - 1 do
    { this will also destroy previous List[I],
      if StartGame is not called for 1st time.
      And that's Ok, we want it. }
    List[I] := TExplosion.Create;
end;

end.
