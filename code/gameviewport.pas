{
  Copyright 2016-2022 Michalis Kamburelis.

  This file is part of "Muuu".

  "Muuu" is free software; see the file COPYING.txt,
  included in this distribution, for details about the copyright.

  "Muuu" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  ----------------------------------------------------------------------------
}

{ Viewport. }
unit GameViewport;

interface

uses CastleWindow, CastleVectors, CastleRectangles;

var
  Window: TCastleWindow;

const
  ViewportHeight = 1000;
  GrassY = 200;

function ViewportWidth: Single;
function ScreenLeftBottom: TVector2;
function ViewportScale: Single;
function ViewportToWindow(const R: TFloatRectangle): TFloatRectangle;
function ViewportToWindow(const P: TVector2): TVector2;
function WindowToViewport(const P: TVector2): TVector2;

implementation

uses SysUtils, Math,
  CastleFilesUtils, CastleLog, CastleUtils,
  GamePlayer;

function ScreenLeftBottom: TVector2;
begin
  Result := Vector2(Player.Position.X - ViewportWidth / 2, 0);
end;

function ViewportScale: Single;
begin
  Result := Window.Width / ViewportWidth;
end;

function ViewportToWindow(const R: TFloatRectangle): TFloatRectangle;
begin
  Result := FloatRectangle(
    (R.Left - ScreenLeftBottom.X) * ViewportScale,
    R.Bottom * ViewportScale,
    R.Width * ViewportScale,
    R.Height * ViewportScale);
end;

function ViewportToWindow(const P: TVector2): TVector2;
begin
  Result.X := (P.X - ScreenLeftBottom.X) * ViewportScale;
  Result.Y := P.Y * ViewportScale;
end;

function WindowToViewport(const P: TVector2): TVector2;
begin
  Result.X := P.X / ViewportScale + ScreenLeftBottom.X;
  Result.Y := P.Y / ViewportScale;
end;

function ViewportWidth: Single;
begin
  Result := ViewportHeight * Window.Width / Window.Height;
end;

end.
