{
  Copyright 2016-2017, 2022 Michalis Kamburelis.

  This file is part of "Muuu".

  "Muuu" is free software; see the file COPYING.txt,
  included in this distribution, for details about the copyright.

  "Muuu" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  ----------------------------------------------------------------------------
}

{ Implements the game logic, independent from mobile / standalone. }
unit Game;

interface

uses CastleWindow;

implementation

uses SysUtils, Math,
  CastleScene, CastleControls, CastleFilesUtils, CastleKeysMouse,
  CastleLog, CastleUtils, CastleUIControls, CastleColors,
  CastleGLImages, CastleVectors, CastleRectangles, CastleGLUtils,
  CastleTimeUtils, CastleFonts, CastleMessages,
  GamePlayer, GameViewport, GameUtils, GameExplosions, GameRockets,
  GameCows, GameBackground, GameFontScore, GameFont_Score;

{ basic state ---------------------------------------------------------------- }

var
  ScoreLabel: TCastleLabel;
  TutorialDone: boolean;

{ window and application ----------------------------------------------------- }

procedure StartGame;
begin
  WorldTime := 0;

  FreeAndNil(Player);
  Player := TPlayer.Create;
  Player.Position := Vector2(0, GrassY);

  Explosions.StartGame;
  Rockets.StartGame;
  Cows.StartGame;
end;

{ One-time initialization of resources. }
procedure ApplicationInitialize;
begin
  SimpleBackground := TCastleRectangleControl.Create(Application);
  SimpleBackground.Color := Black;
  SimpleBackground.FullSize := true;
  Window.Controls.InsertBack(SimpleBackground);

  ImageGrassForeground := TDrawableImage.Create('castle-data:/grass_foreground.png');
  ImageGrassBackground := TDrawableImage.Create('castle-data:/grass_background.png');
  ImageBgHills := TDrawableImage.Create('castle-data:/bg_hills_repeat.png');
  ImageBgCity := TDrawableImage.Create('castle-data:/bg_city_repeat.png');
  ImageTree := TDrawableImage.Create('castle-data:/tree_smaller.png');

  Window.Controls.InsertFront(TUnder.Create(Application));

  Cows := TCows.Create(Application);
  Window.Controls.InsertFront(Cows);

  Rockets := TRockets.Create(Application);
  Window.Controls.InsertFront(Rockets);

  Explosions := TExplosions.Create(Application);
  Window.Controls.InsertFront(Explosions);

  Window.Controls.InsertFront(TPlayerLayer.Create(Application));

  Window.Controls.InsertFront(TAbove.Create(Application));

  FontScore := TFontScore.Create(Application);
  (FontScore as TCastleFont).Load(TextureFont_LatoRegular_300);

  ScoreLabel := TCastleLabel.Create(application);
  ScoreLabel.Anchor(hpRight, -10);
  ScoreLabel.Anchor(vpTop, -10);
  ScoreLabel.Color := White;
  ScoreLabel.Outline := 1;
  ScoreLabel.OutlineHighQuality := true;
  ScoreLabel.OutlineColor := Green;
  ScoreLabel.FontSize := 100;
  ScoreLabel.CustomFont := FontScore;
  Window.Controls.InsertFront(ScoreLabel);

  Theme.ImagesPersistent[tiWindow].Url := 'castle-data:/WindowDarkTransparent.png';

  StartGame;
end;

procedure WindowUpdate(Container: TUIContainer);
var
  SecondsPassed: TFloatTime;
begin
  SecondsPassed := Container.Fps.SecondsPassed;
  Player.Update(SecondsPassed);
  WorldTime += SecondsPassed;
  ScoreLabel.Caption := IntToStr(Score);

  InCity := Odd(Score div 1000);

  if not TutorialDone then
  begin
    MessageOk(Window,
      'Cows are your friends!' + NL +
      'Protect them, to get the milk!' + NL +
      'MUUUUUUUUUUU!' + NL +
      NL +
      'Hints:' + NL +
      '- Cows produce milk only when safe on the ground.' + NL +
      '- Cows produce milk faster when you''re near them.' + NL +
      NL +
      'Mobile:' + NL +
      '- drag at the bottom to move' + NL +
      '- drag higher (above the grass) to shoot' + NL +
      NL +
      'Desktop:' + NL +
      '- arrow keys to walk' + NL +
      '- drag left mouse button to walk' + NL +
      '- drag right mouse button to shoot' + NL +
{      NL +
      'Shoot the space ships before they abduct the cows.' + NL +
      NL +
      'Be careful - don''t shoot the flying cows.' + NL +}
       NL +
      'CAN YOU REACH THE CITY? (Score > 1000).', hpLeft);
    TutorialDone := true;
  end;
end;

procedure WindowPress(Container: TUIContainer; const Event: TInputPressRelease);
begin
  if Event.IsKey(keyF5) then
    Window.SaveScreen(FileNameAutoInc(ApplicationName + '_screen_%d.png'));
  if Event.IsKey(keyF4) then
    InCity := not InCity;
end;

function MyGetApplicationName: string;
begin
  Result := 'muuu';
end;

initialization
  { This sets SysUtils.ApplicationName.
    It is useful to make sure it is correct (as early as possible)
    as our log routines use it. }
  OnGetApplicationName := @MyGetApplicationName;

  InitializeLog;

  { initialize Application callbacks }
  Application.OnInitialize := @ApplicationInitialize;

  { create Window and initialize Window callbacks }
  Window := TCastleWindow.Create(Application);
  Application.MainWindow := Window;
  Window.OnUpdate := @WindowUpdate;
  Window.OnPress := @WindowPress;

  Window.Container.UIScaling := usEncloseReferenceSize;
  Window.Container.UIReferenceWidth := 1000;
  Window.Container.UIReferenceHeight := ViewportHeight div 2;
finalization
  FreeAndNil(ImageGrassForeground);
  FreeAndNil(ImageGrassBackground);
  FreeAndNil(ImageBgHills);
  FreeAndNil(ImageBgCity);
  FreeAndNil(Player);
  FreeAndNil(ImageTree);
  FreeAndNil(Explosions);
end.
