{$mode objfpc}{$H+}
{$apptype GUI}
program muuu_standalone;
uses CastleWindow, Game, GameViewport;
begin
  Window.FullScreen := true;
  Application.ParseStandardParameters;
  Window.OpenAndRun;
end.
