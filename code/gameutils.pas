{
  Copyright 2016-2017 Michalis Kamburelis.

  This file is part of "Muuu".

  "Muuu" is free software; see the file COPYING.txt,
  included in this distribution, for details about the copyright.

  "Muuu" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  ----------------------------------------------------------------------------
}

{ Basic utilities and variables. }
unit GameUtils;

interface

uses CastleTimeUtils;

var
  WorldTime: TFloatTime;
  Score: Int64;
  InCity: boolean;

implementation

end.
